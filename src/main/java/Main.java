import com.fasterxml.jackson.core.JsonProcessingException;
import menu.*;
import menu.item.*;
import us.AuthService;
import us.UserView;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws JsonProcessingException {
        Scanner scanner = new Scanner(System.in);
        ViewHelper viewHelper = new ViewHelper(scanner);
        UserView userView = new UserView(viewHelper);
        AuthService authService = new AuthService();

        List<MenuItem> items = List.of(
                new RegisterMenuItem(authService, userView),
                new LoginMenuItem(authService, userView),
                new ShowAllMenuItem(authService, userView),
                new OnlyForSubMenu("Your working space\n", authService, new Menu(
                        List.of(
                                new AddInformationMenuItem(authService, userView),
                                new ShowAllContactsMenuItem(authService, userView),
                                new FindContactMenuItem(authService, userView),
                                new LogoutMenuItem(authService),
                                new ExitMenuItem()
                        ),
                        scanner
                )),
                new ExitMenuItem());
        Menu menu = new Menu(items, scanner);
        menu.run();


    }
}
