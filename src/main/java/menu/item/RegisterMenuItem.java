package menu.item;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import menu.MenuItem;
import us.AuthService;
import us.User;
import us.UserView;


@RequiredArgsConstructor
public class RegisterMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Register\n";
    }

    @Override
    public void run() throws JsonProcessingException {
        User user = userView.readUser();
        try {
            authService.register(user);
        } catch (IllegalArgumentException exception) {
            userView.showError(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return !authService.isAuth();
    }
}
