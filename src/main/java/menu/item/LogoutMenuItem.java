package menu.item;

import lombok.RequiredArgsConstructor;
import menu.MenuItem;
import us.AuthService;

@RequiredArgsConstructor
public class LogoutMenuItem implements MenuItem {
    private final AuthService authService;

    @Override
    public String getName() {
        return "Logout\n";
    }

    @Override
    public void run() {
        authService.exit();
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
