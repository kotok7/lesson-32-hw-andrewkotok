package menu.item;


import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import menu.MenuItem;
import us.AuthService;
import us.UserView;

@RequiredArgsConstructor
public class FindContactMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Find contact\n";
    }

    @Override
    public void run() {
        try {
            authService.findContact();
        } catch (IllegalArgumentException exception) {
            userView.showError(exception.getMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
