package menu.item;


import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import menu.MenuItem;
import us.AuthService;
import us.UserView;

@RequiredArgsConstructor
public class ShowAllContactsMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Show all users with contact information\n";
    }

    @Override
    public void run() {
        try {
            authService.showAllContacts();
        } catch (IllegalArgumentException | JsonProcessingException exception) {
            userView.showError(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
