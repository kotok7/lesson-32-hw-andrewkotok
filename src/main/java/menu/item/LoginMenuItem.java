package menu.item;


import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import menu.MenuItem;
import us.AuthService;
import us.User;
import us.UserView;

@RequiredArgsConstructor
public class LoginMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Login\n";
    }

    @Override
    public void run() {
        User user = userView.loginUser();
        try {
            authService.login(user);
        } catch (IllegalArgumentException | JsonProcessingException exception) {
            userView.showError(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return !authService.isAuth();
    }
}
