package menu.item;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import menu.MenuItem;
import us.AuthService;
import us.UserView;

@RequiredArgsConstructor
public class ShowAllMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;

    @Override
    public String getName() {
        return "Show all users\n";
    }

    @Override
    public void run() throws JsonProcessingException {
        try {
            authService.getAllUsers();
        } catch (IllegalArgumentException exception) {
            userView.showError(exception.getMessage());
        }
    }

    @Override
    public boolean isVisible() {
        return !authService.isAuth();
    }
}
