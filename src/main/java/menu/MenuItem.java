package menu;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface MenuItem {
    String getName();

    void run() throws JsonProcessingException;

    default boolean isFinal() {
        return false;
    }

    default boolean isVisible() {
        return true;
    }
}
