package menu;

import us.AuthService;

public class OnlyForSubMenu extends SubMenu {
    private final AuthService authService;

    public OnlyForSubMenu(String name, AuthService authService, Menu menu) {
        super(name, menu);
        this.authService = authService;
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
