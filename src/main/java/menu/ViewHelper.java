package menu;

import lombok.RequiredArgsConstructor;

import java.util.Scanner;

@RequiredArgsConstructor
public class ViewHelper {
    private final Scanner scanner;

    public String readString(String message) {
        System.out.print(message);
        return scanner.nextLine();
    }

    public void showError(String error) {
        System.err.println(error);
    }
}
