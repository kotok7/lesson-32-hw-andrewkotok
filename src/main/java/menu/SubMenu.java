package menu;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SubMenu implements MenuItem {
    private final String name;
    private final Menu menu;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void run() throws JsonProcessingException {
        menu.run();
    }
}