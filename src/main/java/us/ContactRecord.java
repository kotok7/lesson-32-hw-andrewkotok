package us;

public class ContactRecord {
    private ContactType type;
    private String value;

    public ContactRecord() {
    }

    public void setType(final ContactType type) {
        this.type = type;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ContactRecord{" +
                "type=" + type +
                ", value='" + value + '\'' +
                '}';
    }
}
