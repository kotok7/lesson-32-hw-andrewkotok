package us;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import menu.ViewHelper;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@NoArgsConstructor(force = true)
public class AuthService {
    private User currentUser;
    private String token;
    private final Contact contact = new Contact();
    private final ViewHelper viewHelper = new ViewHelper(new Scanner(System.in));

    private final ContactBookApi contactBookApi = new ContactBookApi(
            HttpClient.newBuilder().build(),
            new ObjectMapper(), TaskLink.URI);
    private final Scanner scanner = new Scanner(System.in);


    public void register(User user) throws JsonProcessingException {
        JsonNode jsonNode = contactBookApi.registerNewUsers(user);
        System.out.println(jsonNode.toPrettyString());
    }

    public void login(User user) throws JsonProcessingException {
        JsonNode jsonNode = contactBookApi.userLogin(user);
        System.out.println(jsonNode.toPrettyString());
        token = jsonNode.get("token").toString().replace("\"", "");
        currentUser = user;
        contact.setName(user.getLogin());

    }

    private ContactType chooseType() {
        ContactType result = null;
        System.out.println("What kind of information you want to change: \n");
        System.out.println("1 - " + ContactType.EMAIL);
        System.out.println("2 - " + ContactType.PHONE);
        int choice = scanner.nextInt();
        scanner.hasNextLine();
        if (choice == 1) {
            result = ContactType.EMAIL;
        } else if (choice == 2) {
            result = ContactType.PHONE;
        }else {
            System.out.println("Please enter ONLY NUMBERS (1 or 2)");
        }
        return result;
    }

    public void addInformation() throws JsonProcessingException {
           addRecordInformation();
        contactBookApi.addInformation(contact, token);
    }

    public void showAllContacts() throws JsonProcessingException {
        contactBookApi.showAllContactInformation(token);
    }

    public void findContact() throws JsonProcessingException {
        System.out.println("1 - Find by name");
        System.out.println("2 - Find by other information");
        int choice = scanner.nextInt();
        scanner.hasNextLine();
        if (choice == 1) {
            String contactName = viewHelper.readString("Enter name of user you want to find: ");
            contact.setName(contactName);
        } else if (choice == 2) {
            addRecordInformation();
        }
        contactBookApi.findContact(contact, token);
    }

    private void addRecordInformation() {
        List<ContactRecord> listOfRecords = new ArrayList<>();
        ContactRecord record = new ContactRecord();
            record.setType(chooseType());
            record.setValue(viewHelper.readString("Enter information: "));
            listOfRecords.add(record);
        contact.setRecords(listOfRecords);
    }

    public void getAllUsers() {
        JsonNode allUsers = contactBookApi.getAllUsers();
        System.out.println(allUsers.toPrettyString());
    }

    public void exit() {
        currentUser = null;
    }

    public boolean isAuth() {
        return currentUser != null;
    }


}
