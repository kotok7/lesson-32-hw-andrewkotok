package us;

import lombok.Data;

import java.util.List;

@Data
public class Contact {
    private Long id;
    private String name;
    private User user;
    private List<ContactRecord> records;

    public Contact() {
    }

    public String getName() {
        return this.name;
    }


    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", records=" + records +
                '}';
    }


    public void setRecords(final List<ContactRecord> records) {
        this.records = records;
    }
}
