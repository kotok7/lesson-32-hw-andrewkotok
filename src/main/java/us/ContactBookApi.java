package us;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ContactBookApi {
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;
    private final String baseUri;


    public ContactBookApi(HttpClient httpClient, ObjectMapper objectMapper, String baseUri) {
        this.httpClient = httpClient;
        this.objectMapper = objectMapper;
        this.baseUri = baseUri;
    }

    public JsonNode getAllUsers() {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .headers("Accept", "application/json")
                .uri(URI.create(baseUri + "/users"))
                .timeout(Duration.of(10, ChronoUnit.SECONDS))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) throw new RuntimeException(getError(response));
            return objectMapper.readTree(response.body());
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getError(HttpResponse<String> response) {
        return Arrays.stream(response.body().split(","))
                .filter(e -> e.startsWith("\"error"))
                .collect(Collectors.joining())
                .concat(", \"status\":" + response.statusCode());
    }

    public JsonNode registerNewUsers(User user) throws JsonProcessingException {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(user)))
                .uri(URI.create(baseUri + "/register"))
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .timeout(Duration.of(10, ChronoUnit.SECONDS))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) throw new RuntimeException(getError(response));
            return objectMapper.readTree(response.body());
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonNode userLogin(User user) throws JsonProcessingException {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(user)))
                .uri(URI.create(baseUri + "/login"))
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .timeout(Duration.of(10, ChronoUnit.SECONDS))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) throw new RuntimeException(getError(response));
            return objectMapper.readTree(response.body());

        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void addInformation(Contact contact, String token) throws JsonProcessingException {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(contact)))
                .uri(URI.create(baseUri + "/contacts/add"))
                .header("Authorization", "Bearer " + token)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .timeout(Duration.of(10, ChronoUnit.SECONDS))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            if (response.statusCode() != 200) throw new RuntimeException(getError(response));
            objectMapper.readTree(response.body());
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    //TODO find
    public void findContact(Contact contact, String token) throws JsonProcessingException {
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(contact)))
                .uri(URI.create(baseUri + "/contacts/find"))
                .header("Authorization", "Bearer " + token)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .timeout(Duration.of(10, ChronoUnit.SECONDS))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            JsonNode node = objectMapper.readTree(response.body());
            System.out.println(node.toPrettyString());
            if (response.statusCode() != 200) throw new RuntimeException(getError(response));
            objectMapper.readTree(response.body());
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void showAllContactInformation(String token) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(baseUri + "/contacts"))
                .header("Authorization", "Bearer " + token)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .timeout(Duration.of(10, ChronoUnit.SECONDS))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            JsonNode node = objectMapper.readTree(response.body());
            System.out.println(node.toPrettyString());
            if (response.statusCode() != 200) throw new RuntimeException(getError(response));
            objectMapper.readTree(response.body());
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }


}

