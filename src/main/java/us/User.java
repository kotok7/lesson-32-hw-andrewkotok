package us;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class User {
    String login;
    String password;
    @JsonProperty("date_born")
    String dateBorn;
}
