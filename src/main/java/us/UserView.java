package us;

import lombok.RequiredArgsConstructor;
import menu.ViewHelper;

@RequiredArgsConstructor
public class UserView {
    private final ViewHelper viewHelper;

    public User readUser() {
        String login = viewHelper.readString("Enter login: ");
        String password = viewHelper.readString("Enter password: ");
        String dateBorn = viewHelper.readString("Enter date of birth (yyyy-MM-dd): ");
        return new User(login, password, dateBorn);
    }

    public User loginUser() {
        String login = viewHelper.readString("Enter login: ");
        String password = viewHelper.readString("Enter password: ");
        return new User(login, password, null);
    }


    public void showError(String error) {
        viewHelper.showError(error);
    }

}
